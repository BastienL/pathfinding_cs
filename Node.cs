﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pathfinding
{
    class Node
    {
        public int x { get; }
        public int y { get; }
        public int value { get; }
        public int nbOfNode { get; }
        public int[] neighbours { get; }
        public bool isStart { get; set; }
        public bool isEnd { get; set; }
        public bool locked { get; set; }

        public Node(int x, int y, int value, int nbOfNode, int[] neighbours)
        {
            this.x = x;
            this.y = y;
            this.value = value;
            this.locked = false;
            this.isStart = false;
            this.isEnd = false;
            this.nbOfNode = nbOfNode;
            this.neighbours = neighbours;
        }
    }
}
