﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pathfinding
{
    class Graph
    {
        public int distance { get; set; }
        public Node fromNode { get; set; }
        public Node toNode { get; set; }
        public bool locked { get; set; }
        public int id { get; set; }
        public int parentId { get; set; }

        public Graph(Node fromNode, Node toNode, int distance, int id, int parentId)
        {
            this.fromNode = fromNode;
            this.distance = distance;
            this.toNode = toNode;
            this.id = id;
            this.parentId = parentId;
        }
    }
}
