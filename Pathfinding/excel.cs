﻿using OfficeOpenXml;
using System;
using System.IO;
using System.Linq;

namespace Pathfinding
{
    public static class Excel
    {
        static string path = "F:/Desktop/Grille.xlsx";
        static FileInfo fileInfo = new FileInfo(path);

        static ExcelPackage package = new ExcelPackage(fileInfo);
        static ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();



        public static char[,] Tableau()
        {
            int rows = worksheet.Dimension.Rows;
            int columns = worksheet.Dimension.Columns;

            char[,] TableauEnChar = new char[rows + 1, columns + 1];

            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= columns; j++)
                {
                    TableauEnChar[i, j] = char.Parse(worksheet.Cells[i, j].Value.ToString());
                }
            }

            return TableauEnChar;
        }

    }
}
