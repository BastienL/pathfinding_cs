﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pathfinding
{
    class Node
    {
        public int x { get; set; }
        public int y { get; set; }
        public int value { get; set; }
        public int nbOfNode { get; set; }
        public int[] neighbours { get; set; }
        public bool isStart { get; set; }
        public bool isEnd { get; set; }
        public bool locked { get; set; }

        public Node(int x, int y, int value, int nbOfNode, int[] neighbours)
        {
            this.x = x;
            this.y = y;
            this.value = value;
            this.nbOfNode = nbOfNode;
            this.neighbours = neighbours;
        }
    }
}
