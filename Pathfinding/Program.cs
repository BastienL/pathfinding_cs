﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pathfinding
{
    class Program
    {
        static void Main(string[] args)
        {
            var areneChar = Excel.Tableau();

            var areneInt = parseMatrixCharToInt(areneChar);

            var nodes = getNodes(areneInt, areneChar);

            var starterNode = nodes.First(n => n.isStart);

            if (starterNode == null)
            {
                Console.Error.WriteLine("No starter point defined");
                return;
            }

            bool isFinished = false;
            var graphTotal = new List<Graph>();
            Node graphToProcess = starterNode;

            int distanceTotal = 0;

            Graph NextElementToProcess = null;

            int id = 0;

            Graph lightestGraph = null;

            Console.WriteLine("------------------ PLEASE WAIT ------------------");

            while(!isFinished)
            {
                var graphLocal = route(graphToProcess, distanceTotal, nodes, ref id, NextElementToProcess);

                graphTotal = graphTotal.Concat(graphLocal).ToList();

                isFinished = true;

                foreach (Graph graph in graphTotal)
                {

                    if (!graph.locked)
                    {
                        isFinished = false;
                        lightestGraph = graph;
                        Console.WriteLine("------------------ LIGTHEST ROAD FOUND ------------------");
                    }

                    if (graph.toNode == graphToProcess)
                    {
                        graph.locked = true;
                    }


                    if (graph.toNode.isEnd)
                    {
                        isFinished = true;
                        lightestGraph = graph;
                        Console.WriteLine("------------------ LIGTHEST ROAD FOUND ------------------");
                        break;
                    }
                }

                NextElementToProcess = (from graph in graphTotal where !graph.locked && !graph.toNode.locked orderby graph.distance select graph).First();

                graphToProcess = NextElementToProcess.toNode;
                graphToProcess.locked = true;
                NextElementToProcess.locked = true;
            }

            Graph graphToprint = lightestGraph;

            Console.WriteLine("Distance total : " + graphToprint.distance);
            Console.WriteLine("Node n° : |  x :  |  y :  ");
            Console.WriteLine(" " + graphToprint.toNode.nbOfNode + "\t\t " + (graphToprint.toNode.x + 1) + "\t\t " + (graphToprint.toNode.y + 1));

            while (graphToprint.parentId != 0)
            {
                Console.WriteLine(" " + graphToprint.fromNode.nbOfNode + "\t\t " + (graphToprint.fromNode.x + 1) + "\t\t " + (graphToprint.fromNode.y + 1));
                graphToprint = graphTotal[graphToprint.parentId - 1];
            }

            Console.WriteLine(" " + graphToprint.fromNode.nbOfNode + "\t\t " + (graphToprint.fromNode.x + 1) + "\t\t " + (graphToprint.fromNode.y + 1));
        }

        private static int[,] parseMatrixCharToInt(char[,] matrix)
        {
            int Y = matrix.GetLength(0), X = matrix.GetLength(1);

            int[,] intMatrix = new int[Y, X];

            for (int x = 0; x < X; x++)
            {
                for (int y = 0; y < Y; y++)
                {
                    switch(matrix[y, x])
                    {
                        case 'S':
                        case 'E':
                            intMatrix[y, x] = 0;
                            break;

                        case 'X':
                            intMatrix[y, x] = -1;
                            break;

                        default:
                            intMatrix[y,x] = matrix[y, x];
                            break;
                    }
                }
            }

            return intMatrix;
        }

        private static List<Node> getNodes(int[,] intMatrix, char[,] charMatrix)
        {
            int value, nbOfNode = -1;
            int maxY = charMatrix.GetLength(0);
            int maxX = charMatrix.GetLength(1);

            List<Node> nodes = new List<Node>();

            for (int y = 0; y < maxY + 1; y++)
            {
                for (int x = 0; x < maxX + 1; x++)
                {
                    int nb = 0;
                    int[] voisin = new int[4];
                    nbOfNode++;
                    value = intMatrix[y,x];

                    if (x - 1 >= 0 && intMatrix[y, x - 1] > -1)
                    {
                        voisin[nb] = nbOfNode - 1;
                        nb++;
                    }

                    if (x + 1 <= maxX && intMatrix[y, x + 1] > -1)
                    {
                        voisin[nb] = nbOfNode + 1;
                        nb++;
                    }

                    if (y - 1 >= 0 && intMatrix[y - 1, x] > -1)
                    {
                        voisin[nb] = nbOfNode - (maxX + 1);
                        nb++;
                    }

                    if (y + 1 <= maxY && intMatrix[y + 1, x] > -1)
                    {
                        voisin[nb] = nbOfNode + maxX + 1;
                        nb++;
                    }

                    Node newNode = new Node(x, y, value, nbOfNode, voisin);

                    if (charMatrix[y,x] == 'S')
                    {
                        newNode.isStart = true;
                    }
                    else if (charMatrix[y,x] == 'E')
                    {
                        newNode.isEnd = true;
                    }

                    nodes.Add(newNode);
                }
            }

            return nodes;
        }

        private static List<Graph> route(Node currenteNode, int distance, List<Node> nodes, ref int id, Graph parent)
        {
            var localGraph = new List<Graph>();

            foreach (int neighbourid in currenteNode.neighbours)
            {
                Node neighbour = nodes[neighbourid];
                int parentId;

                if (neighbour.locked)
                {
                    continue;
                }

                if (parent != null)
                {
                    distance = parent.distance + neighbour.value;
                    parentId = parent.id;
                }
                else
                {
                    parentId = 0;
                    distance = neighbour.value;
                }

                id += 1;

                Graph newGraph = new Graph(currenteNode, neighbour, distance, id, parentId);

                Graph graphOfNeighbour = newGraph;

                localGraph.Add(graphOfNeighbour);

            }

            if (!localGraph.Any())
            {
                currenteNode.locked = true;
            }

            return localGraph;
        }

        private static Graph[] combine(Graph[] a, Graph[] b)
        {
            int length = a.Length + b.Length;
            Graph[] result = new Graph[length];
            Array.Copy(a, 0, result, 0, a.Length);
            Array.Copy(b, 0, result, a.Length, b.Length);
            
            return result;
        }
    }
}
