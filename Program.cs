﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pathfinding
{
    class Program
    {
        static void Main(string[] args)
        {
            var areneChar = Excel.Tableau();

            int maxY = areneChar.GetLength(0) - 1, maxX = areneChar.GetLength(1) - 1;

            /**
             * This change the array format to use it easly after
             * S = 0
             * E = 0
             * X = -1
             */
            var areneInt = parseMatrixCharToInt(areneChar);

            /**
             * Create a List Of node with :
             *  - Its coordonnate X and Y
             *  - If the Node is the start point
             *  - If the Node is the end point
             *  - If the Node is a wall
             *  - The neighbour of the node
             */
            var nodes = getNodes(maxY, maxX, areneInt, areneChar);


            /**
             * Get the starter node to use it in first
             */
            var starterNode = nodes.First(n => n.isStart);

            if (starterNode == null)
            {
                Console.Error.WriteLine("No starter point defined");
                return;
            }

            /**
             * Graph is :
             *  - The parent Node
             *  - The destination Node
             *  - The prise (distance)
             *  - The parent graph id
             *  - Its id
             */
            bool isFinished = false;
            var graphTotal = new List<Graph>();
            Node graphToProcess = starterNode;

            /**
             * From the start, the ditance is always 0
             */
            int distanceTotal = 0;

            Graph NextElementToProcess = null;

            /**
             * The stater Graph is 0
             */
            int id = 0;

            Graph lightestGraph = null;

            Console.WriteLine("------------------ PLEASE WAIT ------------------");

            /**
             * The end is :
             *   - When all graph are locked
             *   - The first graph arraived to the end
             */
            while (!isFinished)
            {
                var graphLocal = route(graphToProcess, distanceTotal, nodes, ref id, NextElementToProcess);

                graphTotal = graphTotal.Concat(graphLocal).ToList();

                isFinished = true;

                foreach (Graph graph in graphTotal)
                {

                    if (!graph.locked)
                    {
                        isFinished = false;
                        lightestGraph = graph;
                        Console.WriteLine("------------------ LIGTHEST ROAD FOUND ------------------");
                    }

                    if (graph.toNode == graphToProcess)
                    {
                        graph.locked = true;
                    }


                    if (graph.toNode.isEnd)
                    {
                        isFinished = true;
                        lightestGraph = graph;
                        Console.WriteLine("------------------ LIGTHEST ROAD FOUND ------------------");
                        break;
                    }
                }

                /**
                 * Take the graph who got the lightest total distance and who's not locked
                 */
                NextElementToProcess = (from graph in graphTotal where !graph.locked && !graph.toNode.locked orderby graph.distance select graph).First();

                graphToProcess = NextElementToProcess.toNode;
                graphToProcess.locked = true;
                NextElementToProcess.locked = true;
            }

            /**
             * There is just the print
             */

            Graph graphToprint = lightestGraph;

            Console.WriteLine("Distance total : " + graphToprint.distance);
            Console.WriteLine("Node n° : |  x :  |  y :  ");
            Console.WriteLine(" " + graphToprint.toNode.nbOfNode + "\t\t " + (graphToprint.toNode.x + 1) + "\t\t " + (graphToprint.toNode.y + 1));

            while (graphToprint.parentId != 0)
            {
                Console.WriteLine(" " + graphToprint.fromNode.nbOfNode + "\t\t " + (graphToprint.fromNode.x + 1) + "\t\t " + (graphToprint.fromNode.y + 1));
                graphToprint = graphTotal[graphToprint.parentId - 1];
            }

            Console.WriteLine(" " + graphToprint.fromNode.nbOfNode + "\t\t " + (graphToprint.fromNode.x + 1) + "\t\t " + (graphToprint.fromNode.y + 1));
        }

        private static int[,] parseMatrixCharToInt(char[,] matrix)
        {
            int Y = matrix.GetLength(0), X = matrix.GetLength(1);

            int[,] intMatrix = new int[Y, X];

            for (int y = 0; y < Y; y++)
            {
                for (int x = 0; x < X; x++)
                {
                    switch(matrix[y, x])
                    {
                        case 'S':
                        case 'E':
                            intMatrix[y, x] = 0;
                            break;

                        case 'X':
                            intMatrix[y, x] = -1;
                            break;

                        default:
                            intMatrix[y,x] = matrix[y, x];
                            break;
                    }

                    if (intMatrix[y,x] == 88)
                    {
                        intMatrix[y,x] = 1;
                    }
                }
            }

            return intMatrix;
        }

        private static List<Node> getNodes(int maxY, int maxX, int[,] intMatrix, char[,] charMatrix)
        {
            int value, nbOfNode = -1;

            var nodes = new List<Node>();

            for (int y = 0; y < maxY + 1; y++)
            {
                for (int x = 0; x < maxX + 1; x++)
                {
                    int nb = 0;
                    int[] voisin = new int[4];
                    nbOfNode++;
                    value = intMatrix[y,x];

                    if (x - 1 >= 0 && intMatrix[y, x - 1] > -1)
                    {
                        voisin[nb] = nbOfNode - 1;
                        nb++;
                    }

                    if (x + 1 <= maxX && intMatrix[y, x + 1] > -1)
                    {
                        voisin[nb] = nbOfNode + 1;
                        nb++;
                    }

                    if (y - 1 >= 0 && intMatrix[y - 1, x] > -1)
                    {
                        voisin[nb] = nbOfNode - (maxX + 1);
                        nb++;
                    }

                    if (y + 1 <= maxY && intMatrix[y + 1, x] > -1)
                    {
                        voisin[nb] = nbOfNode + maxX + 1;
                        nb++;
                    }

                    Node newNode = new Node(x, y, value, nbOfNode, voisin);

                    if (charMatrix[y,x] == 'S')
                    {
                        newNode.isStart = true;
                    }
                    else if (charMatrix[y,x] == 'E')
                    {
                        newNode.isEnd = true;
                    }

                    nodes.Add(newNode);
                }
            }

            return nodes;
        }

        private static List<Graph> route(Node currenteNode, int distance, List<Node> nodes, ref int id, Graph parent)
        {
            var localGraph = new List<Graph>();

            foreach (int neighbourid in currenteNode.neighbours)
            {
                Node neighbour = nodes[neighbourid];
                int parentId;

                if (neighbour.locked)
                {
                    continue;
                }

                if (parent != null)
                {
                    distance = parent.distance + neighbour.value;
                    parentId = parent.id;
                }
                else
                {
                    parentId = 0;
                    distance = neighbour.value;
                }

                id += 1;

                Graph newGraph = new Graph(currenteNode, neighbour, distance, id, parentId);

                Graph graphOfNeighbour = newGraph;

                localGraph.Add(graphOfNeighbour);

            }

            if (!localGraph.Any())
            {
                currenteNode.locked = true;
            }

            return localGraph;
        }

        private static Graph[] combine(Graph[] a, Graph[] b)
        {
            int length = a.Length + b.Length;
            Graph[] result = new Graph[length];
            Array.Copy(a, 0, result, 0, a.Length);
            Array.Copy(b, 0, result, a.Length, b.Length);
            
            return result;
        }
    }
}
