﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pathfinding
{
    class Graph
    {
        public int distance { get; }
        public Node fromNode { get; }
        public Node toNode { get; }
        public bool locked { get; set; }
        public int id { get; }
        public int parentId { get; }

        public Graph(Node fromNode, Node toNode, int distance, int id, int parentId)
        {
            this.fromNode = fromNode;
            this.distance = distance;
            this.toNode = toNode;
            this.id = id;
            this.parentId = parentId;
        }
    }
}
